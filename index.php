<?php
include "config.php";

?>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	 
	<title>Halaman Admin</title>
	<link rel="stylesheet" href="css/bootstrap.min.css"/>
	<link rel="stylesheet" href="css/login.css"/>
	<link href="css/bootstrap2.css" rel="stylesheet" media="screen" />
	<link href="css/datepicker2.css" rel="stylesheet" />
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">

	<!-- Script js -->
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap-datepicker2.js"></script>
	   

	<script> 
		//options method for call datepicker
		$('#datepicker').datepicker({
			 format: 'yyyy-mm-dd',
			 autoclose: true,
			 todayHighlight: true
		})
	</script>
	<!-- End Script -->
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-offset-4 col-md-4 login-from">
            <h4><em class="glyphicon glyphicon-log-in"></em> Login</h4>
 
            <?php 
            /**
             * Pesan Error Bila terjadi kegagalan dalam login
             */
            if (isset($_GET['login']) && $_GET['login'] == 'salah') {
                echo '<div class="alert alert-warning alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Username atau password salah! </strong> 
                       </div>'; 
            }
			if (isset($_GET['login']) && $_GET['login'] == 'error') {
                echo '<div class="alert alert-warning alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Username dan password tidak boleh kosong! </strong> 
                       </div>'; 
            }?>
            <form action="login.php" method="post">
				<div class="form-group">
					<label for="">Kategori</label>
					<select id="level" name="level" class="combobox form-control" required>
							<option value="tbl_admin">Admin</option>
							<option value="tbl_dosen">Dosen</option>
					</select>
				</div>				
                <div class="form-group">
                    <label for="">Username</label>
                    <input type="text" class="form-control" name="username" placeholder="Username"/>
                </div>
                <div class="form-group">
                    <label for="">Password</label>
                    <input type="password" class="form-control" name="password" placeholder="Password" />
                </div>
                <div class="text-right">
                    <button class="btn btn-primary">Login</button>
                </div>
            </form>
               
        </div>
    </div>
</div> <!-- End container -->
 
    <!-- Script js -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- End Script -->
</body>
</html>