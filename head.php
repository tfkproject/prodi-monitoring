<?php 
$master_folder= "emel/";
$master_link = 'http://'.$_SERVER['HTTP_HOST'].'/'. $master_folder.'';
?>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	 
	<title>Halaman Admin</title>
	<link rel="stylesheet" href="<?php echo $master_link?>css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<?php echo $master_link?>css/login.css"/>
	<link href="<?php echo $master_link?>css/bootstrap2.css" rel="stylesheet" media="screen" />
	<link href="<?php echo $master_link?>css/datepicker2.css" rel="stylesheet" />
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">

	<!-- Script js -->
	<script src="<?php echo $master_link?>js/jquery.min.js"></script>
	<script src="<?php echo $master_link?>js/bootstrap.min.js"></script>
	<script src="<?php echo $master_link?>js/jquery.js"></script>
	<script src="<?php echo $master_link?>js/bootstrap-datepicker2.js"></script>
    <script type="text/javascript" src="<?php echo $master_link?>tiny_mce/tiny_mce.js"></script>
	   

	<script> 
		//options method for call datepicker
		$('#datepicker').datepicker({
			 format: 'yyyy-mm-dd',
			 autoclose: true,
			 todayHighlight: true
		})
	</script>
	<!-- End Script -->
</head>