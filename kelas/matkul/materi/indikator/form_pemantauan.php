<?php
session_start();
if (empty($_SESSION['username'])) {
header("location:index.php"); // jika belum login, maka dikembalikan ke file form_login.php
}
else {
	
include "../../../../config.php";

?>
<html lang="en">
<?php include "../../../../head.php";?>
<body style="font-family:Time; font-size:12px" onload="window.print()">
<div class="container-fluid">
	<center><img src="img/kop.png"/></center>
	
    <!-- Tangkap ID-->
    <?php 
	$id_matkul = $_GET['id_matkul'];
	$id_kelas = $_GET['id_kelas'];
	$id_dosen = $_GET['id_dosen'];
		if (empty($id_kelas) || empty($id_dosen) || empty($id_matkul)) {
				header("location:../../../../index.php");
		}
	?>
    
    <div class="row">
    	<div class="col-lg-12">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-12">
					
	<hr>
	<p>
	<center style="font-family:Time; font-size:14px">
	DAFTAR PEMANTAUAN KESESUAIAN CATATAN MATERI PERKULIHAAN TERHADAP
	<br>RPKPS/SAP/RPS DARI MATA KULIAH YANG DIAJARKAN
	<br>( SEMESTER GENAP T.A : 2016 – 2017 )
	</p>
	
	<tbody>
	<tr align='left'>
        <td width="30%">Mata Kuliah</td>
        <td width="10%">:</td>
        <td width="60%">
			<?php
			$id= $_GET['id_matkul'];
			$q = "select * from tbl_matkul where id_matkul = '$id'";
			$h = mysql_query($q);
			while($data = mysql_fetch_array($h)){
				 echo $data['nama_matkul'];
			}
			?>
		</td>
    </tr>
	<br>
	<tr align='left'>
        <td width="30%">Semester</td>
		<td width="10%">:</td>
        <td width="60%">2016/2017 - Genap</td>
    </tr>
	<br>
	<tr align='left'>
        <td width="30%">Dosen Pengampu</td>
		<td width="10%">:</td>
        <td width="60%">
			<?php
			$id= $_GET['id_dosen'];
			$q = "select * from tbl_dosen where id_dosen = '$id'";
			$h = mysql_query($q);
			while($data = mysql_fetch_array($h)){
				 echo $data['nama'];
			}
			?>
		</td>
    </tr>
	<br>
	<tr align='left' >
        <td width="30%">Program Studi</td>
		<td width="10%">:</td>
        <td width="60%">D-3 Teknik Informatika</td>
    </tr>
	<br>
	<tr align='left'>
        <td width="30%">RPKPS/SAP/RPS</td>
		<td width="10%">:</td>
        <td width="60%">(Ada/ Tidak Ada)</td>
    </tr>
	</tbody>
    <br>
	<tr align='left'>
        <td width="30%">Catatan Materi Perkuliahan</td>
		<td width="10%">:</td>
        <td width="60%">(Ada/ Tidak Ada)</td>
    </tr>
	</tbody>
    </center>
	<br>

            <table id="beritai" class="table table-bordered" >
				<!-- kepala tabel -->
                <thead>
                    <tr style="font-family:Time; font-size:8px; font-style: bold;">
                        <th align="center" width="20%"><center>PERTEMUAN/<br>MINGGU KE<br>(TANGGAL KULIAH)</center></th>
                        <th align="center" width="50%">POKOK BAHASAN RPKPS / SAP/ RPS</th>
						<th align="center" width="20%">RPKPS / SAP / RPS SESUAI TERHADAP CMP<br><hr><center>YA&nbsp;&nbsp;|&nbsp;&nbsp;TIDAK</center></th>
                        <th align="center" width="20%"><center>KETERANGAN</center></th>
                    </tr>
                </thead>
				<!-- isi tabel / data / table of content -->
                <tbody>
                    <?php
						//paging
						$per_hal = 10;
						$jumlah_record = mysql_query ("SELECT COUNT(*) FROM tbl_indikator");
						$jum = mysql_result($jumlah_record, 0);
						$halaman = ceil($jum/$per_hal);
						$page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
						$start = ($page - 1) * $per_hal;
						$query = mysql_query("SELECT * FROM tbl_indikator WHERE id_dosen = '$id_dosen' AND id_matkul = '$id_matkul' ORDER by id_indikator ASC LIMIT $start, $per_hal");
						$no = 1;
						while ($row = mysql_fetch_array ($query))
						{
								$id = $row['id_indikator'];
								$parameter = 'id_indikator='.$id.'&'.'id_matkul='.$id_matkul.'&'.'id_kelas='.$id_kelas.'&'.'id_dosen='.$id_dosen;
					?>
 
                    <tr>
						<td><center><?php echo  $row['minggu'];?></center></td>
                        <td><?php echo  $row['indikator']; ?></td>
                        <td><?php echo  $row['kesamaan']; ?></td>
                        <td><?php echo  $row['keterangan']; ?></td>
                    </tr>
                    <?php
                    $no++;
                    }
                    ?>
                </tbody>
            </table>
        </div><!-- end tab pane -->
                
                	
            </div>
            
            <div class="col-lg-2">
            </div>
        </div><!-- end col lg 12 -->
    </div> <!-- end row -->
</div>

 
    
</body>
</html>
<?php
}
?>