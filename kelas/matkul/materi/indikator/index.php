<?php
session_start();
if (empty($_SESSION['username'])) {
header("location:index.php"); // jika belum login, maka dikembalikan ke file form_login.php
}
else {
	
include "../../../../config.php";

?>
<html lang="en">
<?php include "../../../../head.php";?>
<body>
<div class="container-fluid">
	<?php include "../../../../panel.php";?>
	
    <!-- Tangkap ID-->
    <?php 
	$id_matkul = $_GET['id_matkul'];
	$id_kelas = $_GET['id_kelas'];
	$id_dosen = $_GET['id_dosen'];
		if (empty($id_kelas) || empty($id_dosen) || empty($id_matkul)) {
				header("location:../../../../index.php");
		}
	?>
    
    <div class="row">
    	<div class="col-lg-12">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-12">
					
            		<?php include "../../../../menu_admin.php";?>
					
					<p>
					<button type="button" class="btn" onClick="history.back(-1)">Kembali</button>
					<!-- ini tombol print -->
					<a href="form_pemantauan.php?<?php echo 'id_matkul='.$id_matkul.'&'.'id_kelas='.$id_kelas.'&'.'id_dosen='.$id_dosen;?>" target="_blank"><button type="button" class="btn">Print</button></a>
					</p>
            		<h3><span class="glyphicon glyphicon-book"></span> Tabel Indikator</h3>
                    <p>
						<a href="tambah.php?<?php echo 'id_matkul='.$id_matkul.'&'.'id_kelas='.$id_kelas.'&'.'id_dosen='.$id_dosen;?>">
						<button type="button" class="btn btn-success">Tambah</button>
						</a>
					</p>
                <table id="beritai" class="table table-bordered">
				<!-- kepala tabel -->
                <thead>
                    <tr>
                        <th align="center" width="3%">No</th>
                        <th align="center" width="60%">Indikator</th>
						<th align="center" width="10%">Minggu</th>
                        <th width="10%"><center>Action</center></th>
                    </tr>
                </thead>
				<!-- isi tabel / data / table of content -->
                <tbody style="font-family:Verdana, Geneva, sans-serif; font-size:12px">
                    <?php
						//paging
						$per_hal = 10;
						$jumlah_record = mysql_query ("SELECT COUNT(*) FROM tbl_indikator");
						$jum = mysql_result($jumlah_record, 0);
						$halaman = ceil($jum/$per_hal);
						$page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
						$start = ($page - 1) * $per_hal;
						$query = mysql_query("SELECT * FROM tbl_indikator WHERE id_dosen = '$id_dosen' AND id_matkul = '$id_matkul' ORDER by id_indikator ASC LIMIT $start, $per_hal");
						$no = 1;
						while ($row = mysql_fetch_array ($query))
						{
								$id = $row['id_indikator'];
								$parameter = 'id_indikator='.$id.'&'.'id_matkul='.$id_matkul.'&'.'id_kelas='.$id_kelas.'&'.'id_dosen='.$id_dosen;
					?>
 
                    <tr align='left'>
                        <td><?php echo  $no;?></td>
                        <td><?php echo  $row['indikator']; ?></td>
						<td><?php echo  $row['minggu'];?></td>
                        <td>
							<center>
                            <a href="edit.php?<?php echo  $parameter; ?>"<span class="glyphicon glyphicon-pencil"></span></a> | 
							<a href="hapus.php?<?php echo  $parameter; ?>" onclick="return confirm('Yakin ingin menghapus?');"><span class="glyphicon glyphicon-remove"></span></a>
							</center>							
                        </td>
						
                    </tr>
                    <?php
                    $no++;
                    }
                    ?>
                </tbody>
            </table>  
            <ul class="pagination pagination-sm">
				<li><a href="index.php?page=<?php echo $page -1 ?>"> Previous </a></li>
					</ul>
						<?php
							for($x=1;$x<=$halaman;$x++)
						{
						?>
						<ul class="pagination pagination-sm">
						<li><a href="index.php?page=<?php echo $x ?>"><?php echo $x ?></a></li>
						</ul>
						<?php
						}
						?>
						<ul class="pagination pagination-sm">
							<li><a href="index.php?page=<?php echo $page +1 ?>"> Next </a></li>
						</ul>
            
        </div><!-- end tab pane -->
                
                	
            </div>
            
            <div class="col-lg-2">
            </div>
        </div><!-- end col lg 12 -->
    </div> <!-- end row -->
</div>

 
    
</body>
</html>
<?php
}
?>