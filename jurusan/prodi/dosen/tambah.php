<?php
session_start();
if (empty($_SESSION['username'])) {
header("location:index.php"); // jika belum login, maka dikembalikan ke file form_login.php
}
else {
	
include "../../../config.php";

?>
<html lang="en">
<?php include "../../../head.php";?>
<body>
<div class="container-fluid">
	<?php include "../../../panel.php";?>
    
    <div class="row">
    	<div class="col-lg-12">
        	<h1> Tambah Dosen </h1>
            <form class="form-horizontal" method="post" action="tambah_sender.php?<?php echo 'id_prodi='.$_GET['id_prodi'].'&'.'id_jurusan='.$_GET['id_jurusan'];?>" enctype="multipart/form-data">
            <div class="form-group">
            	<label class="control-label col-xs-3" for="inputid" hidden="true">Id Dosen</label>
                <div class="col-xs-6">
                	<input name="id_dosen" id="id_dosen" class="form-control" type="hidden" value="<?php echo $data['id_dosen']; ?>" placeholder="Id Dosen">
            	</div>
            </div>
                   
            <div class="form-group">
            	<label class="control-label col-xs-3" for="">Nama</label>
                <div class="col-xs-6">
                	<input name="nama" id="nama" class="form-control" value="" id="inputjudul" placeholder="Nama Dosen" required>
            	</div>
            </div>
			
			<div class="form-group">
            	<label class="control-label col-xs-3" for="">Username</label>
                <div class="col-xs-6">
                	<input name="username" id="username" class="form-control" value="" id="inputjudul" placeholder="Username" required>
            	</div>
            </div>
			
			<div class="form-group">
            	<label class="control-label col-xs-3" for="">Password</label>
                <div class="col-xs-6">
                	<input name="password" id="password" class="form-control" value="" id="inputjudul" placeholder="Password" required>
            	</div>
            </div>
				            
            <div class="form-group">
            <label class="control-label col-xs-3" for="inputstatus"></label>
            	<div class="col-xs-6">
            	<p align="left">
            	<button type="submit"  name="simpan" id="simpan" class="btn  btn-primary">Simpan</button> &nbsp;&nbsp;&nbsp;
                <button type="button" class="btn btn-primary" onClick="history.back(-1)">Cancel</button>
               </p>
                </div>
            </div>
            </form>
       	</div><!-- end class col 12 -->
       
    </div> <!-- end row -->
</div>

<script type="text/javascript">
 tinyMCE.init({
 // General options
 mode : "textareas",
 theme : "advanced",
 plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",
 
 // Theme options
 theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
 theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
 theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
 theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
 theme_advanced_toolbar_location : "top",
 theme_advanced_toolbar_align : "left",
 theme_advanced_statusbar_location : "bottom",
 theme_advanced_resizing : true,
 
 // Example content CSS (should be your site CSS)
 content_css : "css/content.css",
 
 // Drop lists for link/image/media/template dialogs
 template_external_list_url : "lists/template_list.js",
 external_link_list_url : "lists/link_list.js",
 external_image_list_url : "lists/image_list.js",
 media_external_list_url : "lists/media_list.js",
 
 // Style formats
 style_formats : [
 {title : 'Bold text', inline : 'b'},
 {title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
 {title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
 {title : 'Example 1', inline : 'span', classes : 'example1'},
 {title : 'Example 2', inline : 'span', classes : 'example2'},
 {title : 'Table styles'},
 {title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
 ],
 
 // Replace values for the template plugin
 
 });
</script>

    <script> 
    //options method for call datepicker
	$('#datepicker').datepicker({
         format: 'yyyy-mm-dd',
		 autoclose: true,
		 todayHighlight: true
	})
    </script>
    <!-- End Script -->
</body>
</html>
<?php
}
?>