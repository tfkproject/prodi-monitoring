<?php
session_start();
if (empty($_SESSION['username'])) {
header("location:index.php"); // jika belum login, maka dikembalikan ke file form_login.php
}
else {
	
include "../../../../../../config.php";

?>
<html lang="en">
<?php include "../../../../../../head.php";?>
<body>
<div class="container-fluid">
	<?php include "../../../../../../panel.php";?>
	
	<!-- Tangkap ID-->
    <?php 	$id_matkul = $_GET['id_matkul'];
			$id_kelas = $_GET['id_kelas'];
			$id_jurusan = $_GET['id_jurusan'];
			$id_prodi = $_GET['id_prodi'];
			$id_dosen = $_GET['id_dosen'];
			if (empty($id_jurusan) || empty($id_prodi) || empty($id_dosen)) {
				header("location:../../../../../../index.php");
			}
	?>
	
    <div class="row">
    	<div class="col-lg-12">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-12">
					
            		<?php include "../../../../../../menu_admin.php";?>
					
					<p> <button type="button" class="btn" onClick="history.back(-1)">Kembali</button></p>
            		<h3><span class="glyphicon glyphicon-book"></span> Daftar Materi</h3>
                    <!--<p> <a href="tambah.php?<?php echo 'id_dosen='.$id_dosen.'&'.'id_prodi='.$id_prodi.'&'.'id_jurusan='.$id_jurusan;?>"><button type="button" class="btn btn-success">Tambah</button></a> </p>-->
                <table id="beritai" class="table table-bordered">
                <thead>
                    <tr>
                        <th align="center" width="3%">No</th>
						<th align="center" width="7%">Minggu</th>
                        <th align="center" width="40%">Indikator</th>
                        <th align="center" width="40%">Pokok Bahasan</th>
						<th align="center" width="15%">Tanggal</th>
                        <th width="10%"><center>Validasi</center></th>
                    </tr>
                </thead>
                <tbody style="font-family:Verdana, Geneva, sans-serif; font-size:12px">
                    <?php
						//paging
						$per_hal = 10;
						$jumlah_record = mysql_query ("SELECT COUNT(*) FROM tbl_materi");
						$jum = mysql_result($jumlah_record, 0);
						$halaman = ceil($jum/$per_hal);
						$page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
						$start = ($page - 1) * $per_hal;
						$query = mysql_query("SELECT * FROM `tbl_materi` INNER JOIN `tbl_indikator` ON `tbl_materi`.`id_materi` = `tbl_indikator`.`id_materi`  WHERE `tbl_materi`.`id_dosen` = '$id_dosen' AND `tbl_materi`.`id_matkul` = '$id_matkul' ORDER by `tbl_materi`.`id_materi` DESC LIMIT $start, $per_hal");
						$no = 1;
						while ($row = mysql_fetch_array ($query))
						{
								$id = $row['id_materi'];
								$parameter = 'id_materi='.$id.'&'.'id_matkul='.$id_matkul.'&'.'id_dosen='.$id_dosen.'&'.'id_prodi='.$id_prodi.'&'.'id_jurusan='.$id_jurusan;
					?>
 
                    <tr align='left'>
                        <td><?php echo  $no;?></td>
                        <td><?php echo  $row['minggu'];?></td>
						<td><?php echo  $row['indikator'];?></td>
						<td><?php echo  $row['judul']; ?></td>
						<td><?php echo  $row['tanggal'];?></td>
                        <td>
							<center>
							<?php
								$alert = "return confirm('Beri validasi?');";
								if ($row['status'] == 'selesai'){
								?>
								<a href="cek.php?<?php echo $parameter;?>&status=selesai" onclick="return confirm('Cabut validasi?');"><span style="color: #62c462; font-size: 18px;" class="glyphicon glyphicon-check"></span></a>
								<?php
								}
								?>								
								<?php
								if ($row['status'] == 'belum'){
								?>
								<a href="cek.php?<?php echo $parameter;?>&status=belum" onclick="return confirm('Beri validasi?');"><span style="color: grey; font-size: 18px;" class="glyphicon glyphicon-check"></span></a>
								<?php
								}
							?>							
							</center>							
                        </td>
                    </tr>
                    <?php
                    $no++;
                    }
                    ?>
                </tbody>
            </table>  
            <ul class="pagination pagination-sm">
				<li><a href="index.php?page=<?php echo $page -1 ?>"> Previous </a></li>
					</ul>
						<?php
							for($x=1;$x<=$halaman;$x++)
						{
						?>
						<ul class="pagination pagination-sm">
						<li><a href="index.php?page=<?php echo $x ?>"><?php echo $x ?></a></li>
						</ul>
						<?php
						}
						?>
						<ul class="pagination pagination-sm">
							<li><a href="index.php?page=<?php echo $page +1 ?>"> Next </a></li>
						</ul>
            
        </div><!-- end tab pane -->
                
                	
            </div>
            
            <div class="col-lg-2">
            </div>
        </div><!-- end col lg 12 -->
    </div> <!-- end row -->
</div>

 
    
</body>
</html>
<?php
}
?>