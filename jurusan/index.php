<?php
session_start();
if (empty($_SESSION['username'])) {
header("location:index.php"); // jika belum login, maka dikembalikan ke file form_login.php
}
else {
	
include "../config.php";

?>
<html lang="en">
<?php include "../head.php";?>
<body>
<div class="container-fluid">
	<?php include "../panel.php";?>
    
    <div class="row">
    	<div class="col-lg-12">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-12">
					<!-- manggil menu -->
            		<?php include "../menu_admin.php";?>
										
            		<h3><span class="glyphicon glyphicon-book"></span> Daftar Jurusan</h3>
                    <p> <a href="tambah.php"><button type="button" class="btn btn-success">Tambah</button></a> </p>
                <table id="beritai" class="table table-bordered">
                <!-- kepala tabel -->
				<thead>
                    <tr>
                        <th align="center" width="3%">No</th>
                        <th align="center" width="50%">Nama</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
				<!-- konten atau isi tabel -->
                <tbody style="font-family:Verdana, Geneva, sans-serif; font-size:12px">
                    <?php
						//paging
						$per_hal = 10;
						$jumlah_record = mysql_query ("SELECT COUNT(*) FROM tbl_jurusan");
						$jum = mysql_result($jumlah_record, 0);
						$halaman = ceil($jum/$per_hal);
						$page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
						$start = ($page - 1) * $per_hal;
						$query = mysql_query("SELECT * FROM tbl_jurusan LIMIT $start, $per_hal");
						$no = 1;
						while ($row = mysql_fetch_array ($query))
						{
								$id = $row['id_jurusan'];
								//buat parameter
								$parameter = 'id_jurusan='.$id.'&';
					?>
 
                    <tr align='left'>
                        <td><?php echo  $no;?></td>
						<!-- buat klik per konten untuk bisa lanjut ke bagian dalam / sub folder -->
                        <td><a href="prodi/index.php?<?php echo  $parameter; ?>"><?php echo  $row['nama_jurusan']; ?></a></td>
						
                        <td>
							<center>
							<!-- aksi klik -->
                            <a href="edit.php?<?php echo  $parameter; ?>"<span class="glyphicon glyphicon-pencil"></span></a> | 
							<a href="hapus.php?<?php echo  $parameter; ?>" onclick="return confirm('Yakin ingin menghapus?');"><span class="glyphicon glyphicon-remove"></span></a>
							</center>							
                        </td>
                    </tr>
                    <?php
                    $no++;
                    }
                    ?>
                </tbody>
            </table>  
            <ul class="pagination pagination-sm">
				<li><a href="index.php?page=<?php echo $page -1 ?>"> Previous </a></li>
					</ul>
						<?php
							for($x=1;$x<=$halaman;$x++)
						{
						?>
						<ul class="pagination pagination-sm">
						<li><a href="index.php?page=<?php echo $x ?>"><?php echo $x ?></a></li>
						</ul>
						<?php
						}
						?>
						<ul class="pagination pagination-sm">
							<li><a href="index.php?page=<?php echo $page +1 ?>"> Next </a></li>
						</ul>
            
        </div><!-- end tab pane -->
                
                	
            </div>
            
            <div class="col-lg-2">
            </div>
        </div><!-- end col lg 12 -->
    </div> <!-- end row -->
</div>

 
    
</body>
</html>
<?php
}
?>